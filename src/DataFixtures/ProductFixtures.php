<?php

namespace App\DataFixtures;

use App\Entity\Product;
use App\Entity\Variant;
use App\Entity\Stock;
use App\DataFixtures\SizeFixtures;
use App\DataFixtures\ColorFixtures;
use App\Entity\Color;
use App\Entity\Size;
use App\Entity\StockQuantity;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker;

class ProductFixtures extends Fixture
{

    public const NUMBER_PRODUCT = 50;
    public const NUMBER_VARIANT_PER_PRODUCT = 4;

    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create();
        $colors = $manager->getRepository(Color::class)->findAll();

        // TODO WARN Impossible de charger la fixture via dépendance, obliger de charger à la main
        // TODO à remettre dans SizeFixtures
        /* Textile */
        $sizesKey = array("XS", "S", "M", "L");

        foreach ($sizesKey as $key => $sizeLabel)
        {
            $size = new Size();
            $size->setCategory(Product::CATEGORY_TEXTILE);
            $size->setIndex($key);
            $size->setLabel("" . $sizeLabel);
            $manager->persist($size);
        }

        
        /* Footwear */
        $index = 0;
        for ($i = 38; $i <= 44; $i++)
        {
            $size = new Size();
            $size->setCategory(Product::CATEGORY_FOOTWEAR);
            $size->setIndex($index);
            $size->setLabel("" . $i);
            $manager->persist($size);
            $index++;
        }


        $manager->flush();
        /* TODO End */


        $sizeRepo = $manager->getRepository(Size::class);

        $sizesTextiles = $sizeRepo->findByCategory(Product::CATEGORY_TEXTILE);
        $sizesFootwear = $sizeRepo->findByCategory(Product::CATEGORY_FOOTWEAR);


        /* Random product part */
        for ($i = 1; $i <= self::NUMBER_PRODUCT; $i++)
        {
            /* 
             * Step 1 : Create a product
             */
            $product = $this->quickNewProduct($manager, $faker);
            
            for ($i1 = 1; $i1 <= self::NUMBER_VARIANT_PER_PRODUCT; $i1++)
            {
                /* 
                * Step 2 : Create few variant for that product
                */
                $variant = $this->quickNewVariant($manager, $faker, $product, $colors, $sizesTextiles, $sizesFootwear);

                /* 
                * Step 3 : Create a stock for that variant
                */
                $this->quickNewStock($manager, $faker, $product, $variant, $sizesTextiles, $sizesFootwear);

                /* 
                * Step 4 : Add it in an user's basket
                */

                // TODO
            }
        }

        $manager->flush();
    }

    private function quickNewProduct($manager, $faker)
    {
        $product = new Product();
        $name = implode(' ', $faker->words(
            $faker->numberBetween(1, 4)
        ));
        $name = ucfirst($name);
        $reference = $this->getRandomReference($faker);

        $product->setName($name); // Ex : 'porro sed magni' or 'Optio'
        $product->setCategory($faker->numberBetween(Product::CATEGORY_TEXTILE, Product::CATEGORY_FOOTWEAR));
        $product->setType($faker->randomDigit);
        $product->setReference($reference);
        $product->setBrand($faker->company);
        $product->setGender($faker->numberBetween(1, 3));

        $manager->persist($product);

        return $product;
    }

    // Note : Pas de panique, aucun lien avec la COVID
    private function quickNewVariant($manager, $faker, $product, array $colors, $sizesTextiles, $sizesFootwear)
    {
        $variant = new Variant();

        if ($product->getCategory() == Product::CATEGORY_TEXTILE)
        {
            $variant->addSize($sizesTextiles[array_rand($sizesTextiles)]);
        } else {
            $variant->addSize($sizesFootwear[array_rand($sizesFootwear)]);
        }

        $variant->setReference($product->getReference());

        // Find a random color object in existing array of Color objects
        // (-1 is because array still start at 0)
        $variant->setColor($colors[
            $faker->numberBetween(20, (sizeof($colors) - 1))
        ]);
        $variant->setImage("https://via.placeholder.com/372/431");
        $variant->setPrice($faker->numberBetween(20, 250));
        $variant->setProduct($product);

        $manager->persist($variant);

        return $variant;
    }

    private function quickNewStock($manager, $faker, $product, $variant, array $sizesTextiles, array $sizesFootwear)
    {
        $stock = new Stock();

        $stock->setReference($variant->getReference());
        $stock->setColor($variant->getColor());

        
        if ($product->getCategory() == Product::CATEGORY_TEXTILE)
        {
            $sizesToUse = $sizesTextiles;
        } else {
            $sizesToUse = $sizesFootwear;
        }

        foreach ($sizesToUse as $size)
        {
            $stockQuantity = new StockQuantity();
            $stockQuantity->setStock($stock);
            $stock->addStockQuantity($stockQuantity);
            $stockQuantity->setQuantity($faker->numberBetween(2, 21));
            if ($product->getCategory() == Product::CATEGORY_TEXTILE)
            {
                $stockQuantity->setSize($size);
            } else {
                $stockQuantity->setSize($size);
            }
    
            $manager->persist($stock);
            $manager->persist($stockQuantity);
        }

        return $stock;
    }

    private function getRandomReference($faker, $primaryLength = 6, $secondaryLength = 2)
    {
        $reference = "";

        for ($i = 1; $i <= $primaryLength; $i++)
        {
            $reference .= $faker->randomDigit;
        }

        $reference .= "-";
        
        for ($i = 1; $i <= $secondaryLength; $i++)
        {
            $reference .= $faker->randomDigit;
        }

        return $reference;
    }

    public function getDependencies()
    {
        return [
            SizeFixtures::class,
            ColorFixtures::class,
        ];
    }
}