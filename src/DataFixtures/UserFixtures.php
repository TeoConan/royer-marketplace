<?php

namespace App\DataFixtures;

use App\Entity\Basket;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\User;
use Faker;

class UserFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        /* Static users */
        // Admin
        $this->quickNewUser($manager, "admin@grouperoyer.com", "admin");
        $this->quickNewUser($manager, "teo", "teo");
        $this->quickNewUser($manager, "alexandre.jaunasse@grouperoyer.com", "jaimemeschats");
        
        
        $faker = Faker\Factory::create();

        /* Random users part */
        for ($i = 1; $i <= 50; $i++)
        {
            $user = new User();
            
            $user->setEmail($faker->email);
            $user->setPassword($faker->password);
            
            // TODO Créer directement le Basket au moment de la création de l'utilisateur plutôt qu'à la main
            $basket = new Basket();
            $user->setBasket($basket);

            $manager->persist($user);
            $manager->persist($basket);
        }

        $manager->flush();
    }

    private function quickNewUser($manager, $email, $password)
    {
        $user = new User();
            
        $user->setEmail($email);
        $user->setPassword($password);
        
        
        $basket = new Basket();
        $user->setBasket($basket);

        $manager->persist($user);
        $manager->persist($basket);
    }
}
