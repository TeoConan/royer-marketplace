<?php

namespace App\DataFixtures;

use App\Entity\Color;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker;

class ColorFixtures extends Fixture
{
    public const NUMBER_COLORS = 50;

    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create();

        for ($i = 1; $i <= self::NUMBER_COLORS; $i++)
        {
            $color = new Color();
            $color->setColorCode($i);
            $color->setColorLabel($faker->colorName);
            $manager->persist($color);
        }

        $manager->flush();
    }
}
