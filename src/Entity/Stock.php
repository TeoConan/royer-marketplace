<?php

namespace App\Entity;

use App\Repository\StockRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=StockRepository::class)
 */
class Stock
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $reference;

    /**
     * @ORM\ManyToOne(targetEntity=Color::class, inversedBy="stocks")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Color;

    /**
     * @ORM\OneToMany(targetEntity=StockQuantity::class, mappedBy="stock", orphanRemoval=true)
     */
    private $stockQuantities;

    /**
     * @ORM\OneToMany(targetEntity=BasketItem::class, mappedBy="Stock", orphanRemoval=true)
     */
    private $basketItems;

    public function __construct()
    {
        $this->stockQuantities = new ArrayCollection();
        $this->basketItems = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(string $reference): self
    {
        $this->reference = $reference;

        return $this;
    }

    public function getColor(): ?Color
    {
        return $this->Color;
    }

    public function setColor(?Color $Color): self
    {
        $this->Color = $Color;

        return $this;
    }

    /**
     * @return Collection|StockQuantity[]
     */
    public function getStockQuantities(): Collection
    {
        return $this->stockQuantities;
    }

    public function addStockQuantity(StockQuantity $stockQuantity): self
    {
        if (!$this->stockQuantities->contains($stockQuantity)) {
            $this->stockQuantities[] = $stockQuantity;
            $stockQuantity->setStock($this);
        }

        return $this;
    }

    public function removeStockQuantity(StockQuantity $stockQuantity): self
    {
        if ($this->stockQuantities->removeElement($stockQuantity)) {
            // set the owning side to null (unless already changed)
            if ($stockQuantity->getStock() === $this) {
                $stockQuantity->setStock(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|BasketItem[]
     */
    public function getBasketItems(): Collection
    {
        return $this->basketItems;
    }

    public function addBasketItem(BasketItem $basketItem): self
    {
        if (!$this->basketItems->contains($basketItem)) {
            $this->basketItems[] = $basketItem;
            $basketItem->setStock($this);
        }

        return $this;
    }

    public function removeBasketItem(BasketItem $basketItem): self
    {
        if ($this->basketItems->removeElement($basketItem)) {
            // set the owning side to null (unless already changed)
            if ($basketItem->getStock() === $this) {
                $basketItem->setStock(null);
            }
        }

        return $this;
    }
}
