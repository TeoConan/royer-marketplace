<?php

namespace App\Entity;

use App\Repository\ColorRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ColorRepository::class)
 */
class Color
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $colorCode;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $colorLabel;

    /**
     * @ORM\OneToMany(targetEntity=Variant::class, mappedBy="Color", orphanRemoval=true)
     */
    private $variants;

    /**
     * @ORM\OneToMany(targetEntity=Stock::class, mappedBy="Color", orphanRemoval=true)
     */
    private $stocks;

    public function __construct()
    {
        $this->variants = new ArrayCollection();
        $this->stocks = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getColorCode(): ?int
    {
        return $this->colorCode;
    }

    public function setColorCode(int $colorCode): self
    {
        $this->colorCode = $colorCode;

        return $this;
    }

    public function getColorLabel(): ?string
    {
        return $this->colorLabel;
    }

    public function setColorLabel(string $colorLabel): self
    {
        $this->colorLabel = $colorLabel;

        return $this;
    }

    /**
     * @return Collection|Variant[]
     */
    public function getVariants(): Collection
    {
        return $this->variants;
    }

    public function addVariant(Variant $variant): self
    {
        if (!$this->variants->contains($variant)) {
            $this->variants[] = $variant;
            $variant->setColor($this);
        }

        return $this;
    }

    public function removeVariant(Variant $variant): self
    {
        if ($this->variants->removeElement($variant)) {
            // set the owning side to null (unless already changed)
            if ($variant->getColor() === $this) {
                $variant->setColor(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Stock[]
     */
    public function getStocks(): Collection
    {
        return $this->stocks;
    }

    public function addStock(Stock $stock): self
    {
        if (!$this->stocks->contains($stock)) {
            $this->stocks[] = $stock;
            $stock->setColor($this);
        }

        return $this;
    }

    public function removeStock(Stock $stock): self
    {
        if ($this->stocks->removeElement($stock)) {
            // set the owning side to null (unless already changed)
            if ($stock->getColor() === $this) {
                $stock->setColor(null);
            }
        }

        return $this;
    }
}
