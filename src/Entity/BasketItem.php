<?php

namespace App\Entity;

use App\Repository\BasketItemRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BasketItemRepository::class)
 */
class BasketItem
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Basket::class, inversedBy="basketItems")
     * @ORM\JoinColumn(nullable=false)
     */
    private $basket;

    /**
     * @ORM\Column(type="integer")
     */
    private $quantityAmount;

    /**
     * @ORM\ManyToOne(targetEntity=StockQuantity::class, inversedBy="basketItems")
     * @ORM\JoinColumn(nullable=false)
     */
    private $quantity;

    /**
     * @ORM\ManyToOne(targetEntity=Stock::class, inversedBy="basketItems")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Stock;

    /**
     * @ORM\ManyToOne(targetEntity=Variant::class, inversedBy="basketItems")
     */
    private $Variant;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBasket(): ?Basket
    {
        return $this->basket;
    }

    public function setBasket(?Basket $basket): self
    {
        $this->basket = $basket;

        return $this;
    }

    public function getQuantityAmount(): ?int
    {
        return $this->quantityAmount;
    }

    public function setQuantityAmount(int $quantityAmount): self
    {
        $this->quantityAmount = $quantityAmount;

        return $this;
    }

    public function getQuantity(): ?StockQuantity
    {
        return $this->quantity;
    }

    public function setQuantity(?StockQuantity $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getStock(): ?Stock
    {
        return $this->Stock;
    }

    public function setStock(?Stock $Stock): self
    {
        $this->Stock = $Stock;

        return $this;
    }

    public function getVariant(): ?Variant
    {
        return $this->Variant;
    }

    public function setVariant(?Variant $Variant): self
    {
        $this->Variant = $Variant;

        return $this;
    }
}
