<?php

namespace App\Controller;

use App\Entity\Product;
use App\Entity\Variant;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    #[Route('/', name: 'home')]
    public function index(): Response
    {

        $productRepo = $this->getDoctrine()->getRepository(Product::class);
        $variantRepo = $this->getDoctrine()->getRepository(Variant::class);

        $products = $productRepo->findBy(array(), null, 10);

        $items = array();
        
        foreach ($products as $product)
        {
            $variants = $variantRepo->findByReference($product->getReference());

            foreach ($variants as $variant)
            {
                $item = array(
                    "id" => $variant->getId(),
                    "parentId" => $product->getId(),
                    "name" => $variant->getColor()->getColorLabel() . " - " . $product->getName(),
                    "type" => $product->getType(),
                    "brand" => $product->getBrand(),
                    "image" => $variant->getImage(),
                    "price" => $variant->getPrice(),
                );

                array_push($items, $item);
            }
        }

        shuffle($items);
        
        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
            'items' => $items
        ]);
    }
}
