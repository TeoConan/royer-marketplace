<?php

namespace App\Controller;

use App\Entity\BasketItem;
use App\Repository\UserRepository;
use App\Repository\SizeRepository;
use App\Repository\StockQuantityRepository;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\StockRepository;
use App\Repository\VariantRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BasketController extends AbstractController
{
    const ERROR_NOT_ENOUGH_STOCK = "nostock";

    #[Route('/basket', name: 'basket')]
    public function index(UserRepository $userRepo): Response
    {
        // Forced for user admin : no login system yet
        $adminUser = $userRepo->findOneBy(array("email" => "admin@grouperoyer.com"));

        $adminBasket = $adminUser->getBasket();
        // DB Object list
        $basketItems = $adminBasket->getBasketItems();

        // List for UI
        $listBasketItems = array();
        $totalAmount = 0;
        $date = new \DateTime();
        $date->modify('+1 week');
        $shippingDate = $date->format('l j F / d.m');
        
        foreach ($basketItems as $dbItem)
        {
            $variant = $dbItem->getVariant();
            $product = $variant->getProduct();

            $basketItem = array(
                'id' => $dbItem->getId(),
                'title' => $product->getName(),
                'productLink' => "/product/" . $product->getId() . "/0",
                'image' => $variant->getImage(),
                'type' => $product->getTypeLabel(),
                'reference' => $variant->getReference(),
                'colorLabel' => $variant->getColor()->getColorLabel(),
                'price' => $variant->getPrice(),
                'quantity' => $dbItem->getQuantityAmount(),
                'totalAmount' => ($variant->getPrice() * $dbItem->getQuantityAmount()),
            );

            $totalAmount += $basketItem['totalAmount'];
            array_push($listBasketItems, $basketItem);
        }

        $basketInfos = array(
            'itemCount' => sizeof($listBasketItems),
            'basketItems' => $listBasketItems,
            'totalAmount' => $totalAmount,
            'shippingDate' => $shippingDate,
        );

        return $this->render('basket/index.html.twig', [
            'basketInfos' => $basketInfos,
        ]);
    }

    public function addItem(Request $request,
                            UserRepository $userRepo,
                            VariantRepository $variantRepo,
                            StockRepository $stockRepos,
                            SizeRepository $sizeRepos,
                            StockQuantityRepository $stockQuantityRepos): Response
    {
        // Forced for user admin : no login system yet
        $adminUser = $userRepo->findOneBy(array("email" => "admin@grouperoyer.com"));
        $manager = $this->getDoctrine()->getManager();

        $adminBasket = $adminUser->getBasket();

        //TODO Bug request, impossible de récupérer les variable en $_POST, requête malformée ?
        $content = json_decode($request->getContent());
        $variantId = $content->variantId;
        $quantity = $content->quantity;
        $sizeLabel = strtoupper($content->sizeLabel);
        $colorCode = $content->colorCode;
        // TODO End

        $variant = $variantRepo->find($variantId);

        $size = $sizeRepos->findOneByLabel($sizeLabel);
        $stock = $stockRepos->findByReferenceAndColorCode($variant->getReference(), $colorCode);
        $stockQuantity = $stockQuantityRepos->findOneByStockAndSize($stock, $size);

        // Check if there is enough stock for this product
        if ($stockQuantity->getQuantity() >= $quantity)
        {
            $basketItem = new BasketItem();
            $basketItem->setBasket($adminBasket);
            $basketItem->setStock($stock);
            $basketItem->setQuantity($stockQuantity);
            $basketItem->setQuantityAmount($quantity);
            $basketItem->setVariant($variant);
            $adminBasket->addBasketItem($basketItem);

            $manager->persist($basketItem);
            $manager->persist($adminBasket);

            $manager->flush();

		    return new Response("ok", 200);

        } else {
		    return new Response(self::ERROR_NOT_ENOUGH_STOCK);
        }
    }

    public function removeItem(Request $request, UserRepository $userRepo): Response
    {
        //TODO Bug request, impossible de récupérer les variable en $_POST, requête malformée ?
        $content = json_decode($request->getContent());
        $basketItemId = $content->basketItemId;
        //TODO End

        // Forced for user admin : no login system yet
        $adminUser = $userRepo->findOneBy(array("email" => "admin@grouperoyer.com"));
        $manager = $this->getDoctrine()->getManager();

        $adminBasket = $adminUser->getBasket();
        $basketItems = $adminBasket->getBasketItems()->toArray();

        $basketItem = null;

        foreach($basketItems as $item)
        {
            if ($item->getId() == $basketItemId)
            {
                $basketItem = $item;
                break;
            }
        }

        if ($basketItem == null)
        {
            throw new \Exception("Cannot find and remove basket item " . $basketItemId);
        }

        $manager->remove($basketItem);
        $manager->flush();

		return new Response("ok", 200);
    }
}
