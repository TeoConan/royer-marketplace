<?php

namespace App\Controller;

use App\Entity\Product;
use App\Entity\Variant;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends AbstractController
{
    #[Route('/product', name: 'product')]
    public function index($id, $variantIndex): Response
    {
        $productRepo = $this->getDoctrine()->getRepository(Product::class);
        $variantRepo = $this->getDoctrine()->getRepository(Variant::class);

        $product = $productRepo->find($id);
        $variants = $variantRepo->findByReference($product->getReference());

        $sizesLabels = array();

        if ($product->getCategory() === 1)
        {
            $sizesLabels = array('xs', 's', 'm', 'l');
        } else {
            $sizesLabels = array(38, 39, 40, 41, 42, 43, 44);
        }

        $productInfo = array(
            "variantId" => $variants[$variantIndex]->getId(),
            "name" => $product->getName(),
            "typeLabel" => $product->getTypeLabel(),
            "colorLabel" => $variants[$variantIndex]->getColor()->getColorLabel(),
            "colorCode" => $variants[$variantIndex]->getColor()->getColorCode(),
            "image" => $variants[$variantIndex]->getImage(),
            "isStock" => true,
            "sizesLabels" => $sizesLabels,
            "brand" => $product->getBrand(),
            "image" => $variants[$variantIndex]->getImage(),
            "price" => $variants[$variantIndex]->getPrice(),
        );

        return $this->render('product/index.html.twig', [
            'controller_name' => 'ProductController',
            'product' => $product,
            'variantIndex' => $variantIndex,
            'variants' => $variants,
            'productInfo' => $productInfo
        ]);
    }
}
