<?php

namespace App\Repository;

use App\Entity\Size;
use App\Entity\Stock;
use App\Entity\StockQuantity;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method StockQuantity|null find($id, $lockMode = null, $lockVersion = null)
 * @method StockQuantity|null findOneBy(array $criteria, array $orderBy = null)
 * @method StockQuantity[]    findAll()
 * @method StockQuantity[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StockQuantityRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StockQuantity::class);
    }

    public function findOneByStockAndSize(Stock $stock, Size $size): ?StockQuantity
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.stock = :stock')
            ->andWhere('s.size = :size')
            ->setParameter('stock', $stock->getId())
            ->setParameter('size', $size->getId())
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    // /**
    //  * @return StockQuantity[] Returns an array of StockQuantity objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?StockQuantity
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
