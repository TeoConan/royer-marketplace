# Royer Marketplace

## Prérequis

- Symfony CLI
- Docker
- Composer
- PHP

## Installation et configuration

Installez les dépendances Symfony avec cette commande

```sh
$ composer install
```

Lancez votre Docker PostgresSQL

```sh
$ docker-compose up -d
```
> :warning: Si votre docker ne se lance pas du premier coup, refaite un `docker-compose up -d` Il est possible qu'un problème arrive lors de la première utilisation

Suite à cela, récupérez le port qu'utilise votre Docker (par défault 5432) pour pouvoir éditer votre fichier `.env` 

Modifiez votre fichier `.env` comme ceci :

```yaml
# Url de base
DATABASE_URL="postgresql://db_user:db_password@127.0.0.1:docker_port/db_name?serverVersion=13&charset=utf8"

# Votre url devrait ressembler à ceci, voir le docker-compose pour plus d'informations
DATABASE_URL="postgresql://main:main@127.0.0.1:5432/main?serverVersion=13&charset=utf8"
```

### Migrations

Si votre `.env` est correctement renseigné et que votre Docker est bien accessible, vous allez être en mesure de lancer les migrations de la base de données

```sh
$ php bin/console doctrine:migrations:migrate
```

En cas de soucis, si votre base est déjà pleine et que des conflits arrivent, vous pouvez détruire votre base pour la recréer avec `docker-compose -s rm`, :warning: Attention, cette commande supprimera <u>toutes les données de la base</u>

### Fixtures

Le projet comporte des fixtures composées avec [Faker](https://packagist.org/packages/fzaninotto/faker), pour exécuter les fixtures :

```sh
$ php bin/console doctrine:fixtures:load
```

> :warning: **Faker est un projet abandonné**: Il est possible que les fixtures ne puisse pas être éxécutées en fonction de votre version PHP

### Serveur web

Pour lancer votre serveur web

```sh
$ symfony server:start -d
```

## Usage

Votre site est accessible sur https://localhost:8000 !

> :warning: Attention à ne pas aller sur 127.0.0.1:8000, ceci causera des problèmes de CORS Policy

## Plus d'informations

### MCD

Le MCD de la base à été exporter dans le fichier `diagram-database.jpg ` que voici :

![diagram-database](./diagram-database.jpg)


## Commons issues

Lors de l'exécution de `php bin/console doctrine:migrations:migrate` si vous obtenez :

```
                                                          
  An exception occurred in driver: could not find driver  
                                                          
```

Sur Linux, tenter d'installer `php-pgsql`

```sh
sudo apt install php-pgsql
```

