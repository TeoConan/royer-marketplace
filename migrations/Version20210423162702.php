<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210423162702 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE stock_quantity ADD stock_id INT NOT NULL');
        $this->addSql('ALTER TABLE stock_quantity ADD size_id INT NOT NULL');
        $this->addSql('ALTER TABLE stock_quantity ADD quantity INT NOT NULL');
        $this->addSql('ALTER TABLE stock_quantity ADD CONSTRAINT FK_D0354274DCD6110 FOREIGN KEY (stock_id) REFERENCES stock (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE stock_quantity ADD CONSTRAINT FK_D0354274498DA827 FOREIGN KEY (size_id) REFERENCES size (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_D0354274DCD6110 ON stock_quantity (stock_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_D0354274498DA827 ON stock_quantity (size_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE stock_quantity DROP CONSTRAINT FK_D0354274DCD6110');
        $this->addSql('ALTER TABLE stock_quantity DROP CONSTRAINT FK_D0354274498DA827');
        $this->addSql('DROP INDEX IDX_D0354274DCD6110');
        $this->addSql('DROP INDEX UNIQ_D0354274498DA827');
        $this->addSql('ALTER TABLE stock_quantity DROP stock_id');
        $this->addSql('ALTER TABLE stock_quantity DROP size_id');
        $this->addSql('ALTER TABLE stock_quantity DROP quantity');
    }
}
