<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210412184737 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE basket_item_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE basket_item (id INT NOT NULL, basket_id_id INT NOT NULL, product_id INT NOT NULL, stock_id INT NOT NULL, quantity VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_D4943C2B293CD56D ON basket_item (basket_id_id)');
        $this->addSql('ALTER TABLE basket_item ADD CONSTRAINT FK_D4943C2B293CD56D FOREIGN KEY (basket_id_id) REFERENCES basket (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE basket_item_id_seq CASCADE');
        $this->addSql('DROP TABLE basket_item');
    }
}
