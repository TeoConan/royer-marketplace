<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210421215852 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE basket_item DROP CONSTRAINT FK_D4943C2B293CD56D');
        $this->addSql('DROP INDEX IDX_D4943C2B293CD56D');
        $this->addSql('ALTER TABLE basket_item RENAME COLUMN basket_id_id TO basket_id');
        $this->addSql('ALTER TABLE basket_item ADD CONSTRAINT FK_D4943C2B293CD56D FOREIGN KEY (basket_id) REFERENCES basket (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_D4943C2B293CD56D ON basket_item (basket_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE basket_item DROP CONSTRAINT fk_d4943c2b293cd56d');
        $this->addSql('DROP INDEX idx_d4943c2b293cd56d');
        $this->addSql('ALTER TABLE basket_item RENAME COLUMN basket_id TO basket_id_id');
        $this->addSql('ALTER TABLE basket_item ADD CONSTRAINT fk_d4943c2b293cd56d FOREIGN KEY (basket_id) REFERENCES basket (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_d4943c2b293cd56d ON basket_item (basket_id)');
    }
}
