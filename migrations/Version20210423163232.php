<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210423163232 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE basket_item DROP quantity');
        $this->addSql('ALTER TABLE basket_item ADD quantity_id INT NOT NULL');
        $this->addSql('ALTER TABLE basket_item ADD quantity_amount INT NOT NULL');
        $this->addSql('ALTER TABLE basket_item ADD CONSTRAINT FK_D4943C2B7E8B4AFC FOREIGN KEY (quantity_id) REFERENCES stock_quantity (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_D4943C2B7E8B4AFC ON basket_item (quantity_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE basket_item DROP CONSTRAINT FK_D4943C2B7E8B4AFC');
        $this->addSql('DROP INDEX IDX_D4943C2B7E8B4AFC');
        $this->addSql('ALTER TABLE basket_item DROP quantity_id');
        $this->addSql('ALTER TABLE basket_item DROP quantity_amount');
    }
}
