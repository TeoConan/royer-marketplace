<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210421222911 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER INDEX uniq_2246507b9d86650f RENAME TO UNIQ_2246507BA76ED395');
        $this->addSql('ALTER TABLE basket_item DROP CONSTRAINT FK_D4943C2B293CD56D');
        $this->addSql('DROP INDEX IDX_D4943C2B293CD56D');
        $this->addSql('ALTER TABLE basket_item ADD CONSTRAINT FK_D4943C2B293CD56D FOREIGN KEY (basket_id) REFERENCES basket (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_D4943C2B293CD56D ON basket_item (basket_id)');
        $this->addSql('ALTER TABLE variant ADD color_id INT NOT NULL');
        $this->addSql('ALTER TABLE variant ADD CONSTRAINT FK_F143BFAD7ADA1FB5 FOREIGN KEY (color_id) REFERENCES color (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_F143BFAD7ADA1FB5 ON variant (color_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER INDEX uniq_2246507ba76ed395 RENAME TO uniq_2246507b9d86650f');
        $this->addSql('ALTER TABLE basket_item DROP CONSTRAINT fk_d4943c2b293cd56d');
        $this->addSql('DROP INDEX idx_d4943c2b293cd56d');
        $this->addSql('ALTER TABLE basket_item RENAME COLUMN basket_id_id TO basket_id');
        $this->addSql('ALTER TABLE basket_item ADD CONSTRAINT fk_d4943c2b293cd56d FOREIGN KEY (basket_id) REFERENCES basket (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_d4943c2b293cd56d ON basket_item (basket_id)');
        $this->addSql('ALTER TABLE variant DROP CONSTRAINT FK_F143BFAD7ADA1FB5');
        $this->addSql('DROP INDEX IDX_F143BFAD7ADA1FB5');
        $this->addSql('ALTER TABLE variant DROP color_id');
    }
}
