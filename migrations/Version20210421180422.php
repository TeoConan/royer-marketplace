<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210421180422 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE basket DROP CONSTRAINT FK_2246507B9D86650F');
        $this->addSql('DROP INDEX UNIQ_2246507B9D86650F');
        $this->addSql('ALTER TABLE basket RENAME COLUMN user_id_id TO user_id');
        $this->addSql('ALTER TABLE basket ADD CONSTRAINT FK_2246507B9D86650F FOREIGN KEY (user_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2246507B9D86650F ON basket (user_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE basket DROP CONSTRAINT fk_2246507b9d86650f');
        $this->addSql('DROP INDEX uniq_2246507b9d86650f');
        $this->addSql('ALTER TABLE basket RENAME COLUMN user_id TO user_id_id');
        $this->addSql('ALTER TABLE basket ADD CONSTRAINT fk_2246507b9d86650f FOREIGN KEY (user_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX uniq_2246507b9d86650f ON basket (user_id)');
    }
}
