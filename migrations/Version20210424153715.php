<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210424153715 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE variant_size (variant_id INT NOT NULL, size_id INT NOT NULL, PRIMARY KEY(variant_id, size_id))');
        $this->addSql('CREATE INDEX IDX_1838B2FF3B69A9AF ON variant_size (variant_id)');
        $this->addSql('CREATE INDEX IDX_1838B2FF498DA827 ON variant_size (size_id)');
        $this->addSql('ALTER TABLE variant_size ADD CONSTRAINT FK_1838B2FF3B69A9AF FOREIGN KEY (variant_id) REFERENCES variant (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE variant_size ADD CONSTRAINT FK_1838B2FF498DA827 FOREIGN KEY (size_id) REFERENCES size (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP TABLE variant_size');
    }
}
