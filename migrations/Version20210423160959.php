<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210423160959 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE size_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE stock_quantity_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE size (id INT NOT NULL, index INT NOT NULL, category INT NOT NULL, label VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE stock_quantity (id INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER INDEX idx_d4943c2b293cd56d RENAME TO IDX_D4943C2B1BE1FB52');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE size_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE stock_quantity_id_seq CASCADE');
        $this->addSql('DROP TABLE size');
        $this->addSql('DROP TABLE stock_quantity');
        $this->addSql('ALTER INDEX idx_d4943c2b1be1fb52 RENAME TO idx_d4943c2b293cd56d');
    }
}
